// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/a/PC.hdl

/**
 * A 16-bit counter with load and reset control bits.
 * if      (reset[t] == 1) out[t+1] = 0
 * else if (load[t] == 1)  out[t+1] = in[t]
 * else if (inc[t] == 1)   out[t+1] = out[t] + 1  (integer addition)
 * else                    out[t+1] = out[t]
 */

CHIP PC {
    IN in[16],load,inc,reset;
    OUT out[16];

    PARTS:
    Or8Way(in[0]=reset, in[1]=load, in[2]=inc, in[3..7]=false, out=shouldLoad); // if any of the 3 operations is true the Register should load
    Inc16(in=prevOut, out=incOut);
    Mux16(a=prevOut, b=incOut, sel=inc, out=optionIncrement); // Because of their order if reset == 1 overwrites load option even if it was 1, load overrides inc
    Mux16(a=optionIncrement, b=in, sel=load, out=optionLoad);
    Mux16(a=optionLoad, b=false, sel=reset, out=input);
    Register(in=input, load=shouldLoad, out=prevOut, out=out);
}
